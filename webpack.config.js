const path = require('path');
const Dotenv = require('dotenv-webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = [{
    entry: path.resolve(__dirname, 'src/index.tsx'),
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                enforce: 'pre',
                use: [
                    {
                        options: {
                            eslintPath: require.resolve('eslint')
                        },
                        loader: require.resolve('eslint-loader')
                    }
                ]
            },
            {
                test: /\.(ts|tsx|js|jsx)?$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.(c|sa)ss$/i,
                use: [
                    { loader: MiniCssExtractPlugin.loader },
                    { loader: 'css-loader' },
                    { loader: 'sass-loader' }
                ],
            },
            {
                test: /\.(jpe?g|gif|svgotf|ttf|wav|png|ico)$/i,
                loader: 'url-loader',
                options: {
                    name: 'static/[name].[ext]'
                }
            }
        ]
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx', '.json']
    },
    plugins: [
        // new HtmlWebpackPlugin({
        //     template: 'public/index.html',
        //     hash: true,
        //     filename: 'index.html'
        // }),
        new MiniCssExtractPlugin({
            filename: 'static/[name].css'
        }),
        new Dotenv({
            defaults: true
        })
    ],
    output: {
        filename: 'server.js',
        path: path.resolve(__dirname, 'dist'),
        sourceMapFilename: '[file].map',
        publicPath: path.resolve(__dirname, '/'),

    },
    target: 'node',
    devtool: 'eval-source-map',
    devServer: {
        contentBase: path.resolve(__dirname, 'dist'),
        port: 8080,
        open: false,
        historyApiFallback: true,
        publicPath: 'http://localhost:8080/',
    }
},
{
    entry: path.resolve(__dirname, 'src/client/index.tsx'),
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                enforce: 'pre',
                use: [
                    {
                        options: {
                            eslintPath: require.resolve('eslint')
                        },
                        loader: require.resolve('eslint-loader')
                    }
                ]
            },
            {
                test: /\.(ts|tsx|js|jsx)?$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.(c|sa)ss$/i,
                use: [
                    { loader: MiniCssExtractPlugin.loader },
                    { loader: 'css-loader' },
                    { loader: 'sass-loader' }
                ],
            },
            {
                test: /\.(jpe?g|gif|svgotf|ttf|wav|png|ico)$/i,
                loader: 'url-loader',
                options: {
                    name: 'static/[name].[ext]',
                }
            }
        ]
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx', '.json']
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'public/index.html',
            hash: true,
            filename: 'index.html'
        }),
        new MiniCssExtractPlugin({
            filename: 'static/[name].css'
        })
    ],
    output: {
        filename: 'static/client.js',
        path: path.resolve(__dirname, 'dist'),
        sourceMapFilename: '[file].map',
        publicPath: path.resolve(__dirname, '/'),

    },
    target: 'node',
    devtool: 'eval-source-map'
}];
