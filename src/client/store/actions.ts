import { AppDataPayload } from "./AppDataPayload";

export type ActionType = UserAction | LedgerAction | NullAction;

export enum UserAction {
    SET_USER_ERROR = 'SET_USER_ERROR',
    SET_LOGIN_SUCCESS = 'SET_LOGIN_STATUS',
    SET_LOGOUT = 'SET_LOGOUT',
    SET_ROUTE = 'SET_ROUTE'

}

export enum LedgerAction {
    SET_LEDGER_ERROR = 'SET_LEDGER_ERROR',
    SET_CATEGORIES = 'SET_CATEGORIES',
    SET_FREQUENCIES = 'SET_FREQUENCIES',
    SET_TRANSACTION_TYPES = 'SET_TRANSACTION_TYPES',
    SET_INCOME_GENERATORS = 'SET_INCOME_GENERATORS',
    SET_LEDGER_ENTRIES = 'SET_LEDGER_ENTRIES',
    SET_EXPENSES = 'SET_RECURRING_TRANSACTIONS',
    REMOVE_LEDGER_ENTRY = 'REMOVE_LEDGER_ENTRY',
    REMOVE_EXPENSE = 'REMOVE_EXPENSE',
    REMOVE_INCOME_GENERATOR = 'REMOVE_INCOME_GENERATOR'
}


export enum NullAction {
    NULL_ACTION = 'NULL_ACTION'
}

export type StoreAction = { type: ActionType, payload: Partial<AppDataPayload> };