import IExpense from "../../models/IExpense";
import IFrequency from "../../models/IFrequency";
import IIncomeGenerator from "../../models/IIncomeGenerator";
import ILedgerEntry from "../../models/ILedgerEntry";
import ITransactionType from "../../models/ITransactionType";
import { expensesWithoutGenerators } from "../../utilities/expenses";
import { sortLedgerEntries } from "../../utilities/ledger_entries";
import { sortFrequencies } from "../../utilities/utilities";
import { ActionType, LedgerAction } from "../actions";
import { AppDataPayload } from "../AppDataPayload";

// TODO (alexa): Should some of these arrays be Records instead?
export interface LedgerState {
    error: string;
    categories: string[];
    frequencies: IFrequency[];
    transactionTypes: ITransactionType[];
    incomeGenerators: IIncomeGenerator[];
    ledgerEntries: ILedgerEntry[];
    expenses: IExpense[];
}

const defaultState: LedgerState = {
    error: '',
    categories: [],
    frequencies: [],
    transactionTypes: [],
    incomeGenerators: [],
    ledgerEntries: [],
    expenses: []
};

export const LedgerReducer = (state: LedgerState = defaultState, action: { type: ActionType, payload: AppDataPayload }) => {
    switch (action.type) {
        case LedgerAction.SET_LEDGER_ERROR:
            state = {
                ...state,
                error: action.payload.errorMessage
            };
            break;
        case LedgerAction.SET_CATEGORIES:
            state = {
                ...state,
                categories: [...action.payload.categories].sort()
            };
            break;
        case LedgerAction.SET_FREQUENCIES:
            state = {
                ...state,
                frequencies: sortFrequencies(action.payload.frequencies)
            };
            break;
        case LedgerAction.SET_TRANSACTION_TYPES:
            state = {
                ...state,
                transactionTypes: action.payload.transactionTypes
            };
            break;
        case LedgerAction.SET_INCOME_GENERATORS:
            state = {
                ...state,
                incomeGenerators: action.payload.incomeGenerators,
            };
            break;
        case LedgerAction.SET_LEDGER_ENTRIES:
            state = {
                ...state,
                ledgerEntries: sortLedgerEntries(action.payload.entries)
            };
            break;
        case LedgerAction.SET_EXPENSES:
            state = {
                ...state,
                expenses: expensesWithoutGenerators(action.payload.expenses)
            };
            break;
        case LedgerAction.REMOVE_LEDGER_ENTRY:
            state = {
                ...state,
                ledgerEntries: state.ledgerEntries.filter(x => x.id !== action.payload.id)
            };
            break;
        case LedgerAction.REMOVE_INCOME_GENERATOR:
            state = {
                ...state,
                incomeGenerators: state.incomeGenerators.filter(x => x.id !== action.payload.id)
            };
            break;
        case LedgerAction.REMOVE_EXPENSE:
            state = {
                ...state,
                expenses: state.expenses.filter(x => x.id !== action.payload.id)
            };
            break;
    }
    return state;
}