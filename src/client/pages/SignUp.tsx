import { push } from "connected-react-router";
import React from "react";
import { useState } from "react"
import { connect } from "react-redux"
import UserInfo from "../components/UserInfo";
import AppDataState from "../store/AppDataState";
import { clearUserError, createUser } from "../store/user/actions";
import { MINIMUM_PASSWORD_LENGTH } from "../utilities/constants";

interface ISignUpProps {
    createUser: typeof createUser;
    clearUserError: typeof clearUserError;
    push: typeof push;
}

const SignUp = (props: ISignUpProps) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [processing, setProcessing] = useState(false);

    const onSignUpClick = async () => {
        props.clearUserError();
        setProcessing(true);
        await props.createUser({
            username: username,
            password: password
        });
        setProcessing(false);
        push('/dashboard');
    }

    const signUpDisabled: boolean = processing || !username || password.length < MINIMUM_PASSWORD_LENGTH;
    const buttonText = processing ? 'Signing up' : 'Sign up';

    return (
        <div className="sign-up">
            <h1>Sign up</h1>
            <UserInfo disabled={signUpDisabled} username={username} password={password} handleUsernameChanged={value => setUsername(value)} handlePasswordChanged={setPassword} onSubmit={onSignUpClick} buttonText={buttonText} />
        </div>
    );
}

const mapStateToProps = (state: AppDataState): Partial<ISignUpProps> => {
    return {};
}

export default connect(mapStateToProps, { createUser, clearUserError, push })(SignUp as any);