import { push } from "connected-react-router";
import { useState } from "react";
import { connect } from "react-redux";
import AutocompleteField from "../components/AutocompleteField";
import { addLedgerEntry, getCategories, getTransactionTypes } from "../store/ledger/actions";
import { usesTransactionTypes } from "../utilities/hooks";
import { categoryValidator, numericValidator } from "../utilities/validators";
import React from "react";
import Dropdown, { IDropdownOption } from "../components/Dropdown";
import TextField from "../components/TextField";
import DateField from "../components/DateField";
import ITransactionType from "../models/ITransactionType";

// @ts-expect-error - The typescript compiler says that it can't find 
// the module for this import. Webpack still compiles it just fine.
import add from '../assets/add.png';
import AppDataState from "../store/AppDataState";

interface AddLedgerEntryProps {
    categories: string[];
    transactionTypes: ITransactionType[];
    getCategories: typeof getCategories;
    getTransactionTypes: typeof getTransactionTypes;
    addLedgerEntry: typeof addLedgerEntry;
    push: typeof push;
}

const AddLedgerEntry = (props: AddLedgerEntryProps) => {
    const [transactionType, setTransactionType] = useState('');
    const [category, setCategory] = useState('');
    const [description, setDescription] = useState('');
    const [amount, setAmount] = useState('');
    const [date, setDate] = useState(new Date());

    usesTransactionTypes(props.transactionTypes, props.getTransactionTypes);

    const handleDateChanged = (date: Date | [Date, Date] | null) => {
        if (date instanceof Date) {
            setDate(date);
        }
    }

    // Prevent users from entering a date too far in the past. This prevents 
    // needing to check for negative dates (.getTime() returns negative numbers
    // for dates prior to 1970, for example). This also just simplifies use-cases.
    const getMinDate = (): Date => {
        const date: Date = new Date();
        date.setMonth(date.getMonth() - 1);
        return date;
    }

    // const addTransactionDisabled = (): boolean => {
    //     const numberAmount: number = parseFloat(amount);
    //     return !transactionType
    //         || !category
    //         // description is optional.
    //         || isNaN(numberAmount) || numberAmount <= 0
    //         || !date;
    // }

    const onAddTransactionClick = async () => {
        await props.addLedgerEntry({
            category: category,
            description: description,
            amount: parseFloat(amount),
            transactionTypeId: transactionType,
            recurringTransactionId: '',
            transactionDate: date
        });
        props.push('/dashboard');
    }

    const transactionTypes: IDropdownOption<string>[] = props.transactionTypes.map(x => ({ displayValue: x.description, value: x.id }));

    return (
        <div className="add-transaction">
            <h1 className="title">Add transaction</h1>
            <Dropdown label="Transaction Type" value={transactionType} options={transactionTypes} onChange={setTransactionType} />
            <AutocompleteField label="Category" value={category} options={props.categories} onChange={setCategory} getOptions={props.getCategories} validator={categoryValidator} />
            <TextField label="Description" value={description} onChange={(value) => setDescription(value)} />
            <TextField label="Amount" validator={numericValidator} preToken="$" value={amount} onChange={setAmount} />
            <DateField label="Transaction date" value={date} onChange={handleDateChanged} minDate={getMinDate()} maxDate={new Date()} />

            <div className="add" >
                <img src={add} alt="Add" onClick={onAddTransactionClick} />
            </div>
        </div>
    );
}

const mapStateToProps = (state: AppDataState): Partial<AddLedgerEntryProps> => {
    return {
        categories: state.ledger.categories,
        transactionTypes: state.ledger.transactionTypes
    };
}

export default connect(mapStateToProps, { getCategories, getTransactionTypes, addLedgerEntry, push })(AddLedgerEntry as any);