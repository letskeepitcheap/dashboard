import { push } from "connected-react-router";
import React, { useEffect } from "react";
import { useState } from "react";
import { connect } from "react-redux";
import UserInfo from "../components/UserInfo";
import { IUserInfo } from "../models/IUserInfo";
import AppDataState from "../store/AppDataState";
import { clearUserError, login, refreshLogin, setRoute } from "../store/user/actions";
import { MINIMUM_PASSWORD_LENGTH } from "../utilities/constants";

interface ILoginProps {
    user: IUserInfo | undefined;
    route: string;
    refreshLogin: typeof refreshLogin;
    clearUserError: typeof clearUserError;
    login: typeof login;
    setRoute: typeof setRoute;
    push: typeof push;
}

const Login = (props: ILoginProps) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [processing, setProcessing] = useState(false);

    const forward = () => {
        if (props.route) {
            props.push(props.route);
            props.setRoute('');
        } else {
            props.push('/dashboard');
        }
    }

    useEffect(() => {
        if (props.user) {
            forward();
        }
    }, [props.user])


    const onLoginClick = async () => {
        props.clearUserError();
        setProcessing(true);
        props.login({
            username: username,
            password: password
        });
    }

    const buttonText = processing ? 'Logging in' : 'Log in';
    const loginDisabled: boolean = processing || !username || password.length < MINIMUM_PASSWORD_LENGTH;

    return (
        <div className="login">
            <h1>Login</h1>
            <UserInfo disabled={loginDisabled} username={username} password={password} handleUsernameChanged={setUsername} handlePasswordChanged={setPassword} onSubmit={onLoginClick} buttonText={buttonText} />
        </div>
    );
}

const mapStateToProps = (state: AppDataState): Partial<ILoginProps> => {
    return {
        user: state.user.user
    };
}

export default connect(mapStateToProps, { refreshLogin, clearUserError, login, setRoute, push })(Login as any);