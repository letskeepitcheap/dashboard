import React from 'react';

const About = () =>
    <div className="about">
        <h1>Credits</h1>
        <ul>
            <li>
                Favicon made by <a href="https://www.flaticon.com/authors/kiranshastry" title="Kiranshastry">Kiranshastry</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
            </li>
            <li>
                Down arrow icon based on icon by <a href="https://www.flaticon.com/authors/roundicons" title="Roundicons">Roundicons</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
            </li>
        </ul>
    </div>;

export default About;