import { push } from "connected-react-router";
import { useMemo, useState } from "react"
import { connect } from "react-redux";
import AutocompleteField from "../components/AutocompleteField";
import { addIncomeGenerator, getCategories, getFrequencies, getTransactionTypes } from "../store/ledger/actions";
import { getDateFromFrequency } from "../utilities/dates";
import { usesFrequencies, usesTransactionTypes } from "../utilities/hooks";
import { categoryValidator, descriptionValidator, numericValidator } from "../utilities/validators";
import { getDisplayText } from "../utilities/text";
import React from "react";
import IFrequency from "../models/IFrequency";
import ITransactionType from "../models/ITransactionType";
import IExpenseRequest from "../models/requests/IExpenseRequest";
import Dropdown, { IDropdownOption } from "../components/Dropdown";
import TextField from "../components/TextField";
import DateField from "../components/DateField";
import AppDataState from "../store/AppDataState";

// @ts-expect-error - The typescript compiler says that it can't find 
// the module for this import. Webpack still compiles it just fine.
import close from '../assets/close.png';
// @ts-expect-error - The typescript compiler says that it can't find 
// the module for this import. Webpack still compiles it just fine.
import add from '../assets/add.png';

interface IAddIncomeGeneratorProps {
    categories: string[];
    frequencies: IFrequency[];
    transactionTypes: ITransactionType[];
    getCategories: typeof getCategories;
    getFrequencies: typeof getFrequencies;
    getTransactionTypes: typeof getTransactionTypes;
    addIncomeGenerator: typeof addIncomeGenerator;
    push: typeof push;
}

// TODO (alexa): add validation to ensure that the expenses from the income
// generator don't exceed the income.

const AddIncomeGenerator = (props: IAddIncomeGeneratorProps) => {
    const [jobTitle, setJobTitle] = useState('');
    const [grossPay, setGrossPay] = useState('');
    const [category, setCategory] = useState('');
    const [frequencyId, setFrequencyId] = useState('');
    const [transactionDescription, setTransactionDescription] = useState('');
    const [amount, setAmount] = useState('');
    const [expenses, setExpenses] = useState([] as IExpenseRequest[]);
    const [showDeductionDetails, setShowDeductionDetails] = useState(false);
    const [lastTriggered, setLastTriggered] = useState(new Date());


    usesFrequencies(props.frequencies, props.getFrequencies);
    usesTransactionTypes(props.transactionTypes, props.getTransactionTypes);

    // Minimum date is dependent on the selected frequency.
    const minDate = useMemo(() => getDateFromFrequency(frequencyId, props.frequencies), [frequencyId, props.frequencies]);

    const getTransactionTypeId = (description: string): string =>
        props.transactionTypes.length > 0
            ? props.transactionTypes.find(x => x.description === description)!.id
            : '';


    const incomeTransactionTypeId = getTransactionTypeId('Income');
    const expenditureTransactionTypeId = getTransactionTypeId('Expenditure');

    const handleDateChanged = (date: Date | [Date, Date] | null) => {
        if (date instanceof Date) {
            setLastTriggered(date);
        }
    }

    const onDeleteClick = (value: string) => setExpenses(expenses.filter(x => x.description !== value));

    // const addDisabled = (): boolean => {
    //     const numberAmount: number = parseFloat(amount);
    //     return !category
    //         // description is optional
    //         || isNaN(numberAmount) || numberAmount <= 0;
    // }

    const onAddClick = () => {
        const expense: IExpenseRequest = {
            category: category,
            description: transactionDescription,
            amount: parseFloat(amount),
            frequencyId: '',    // This will be set when the entire request is generated.
            transactionTypeId: expenditureTransactionTypeId,
            lastTriggered: new Date()
        };
        setExpenses([...expenses, expense]);
        // Clear our entries.
        setCategory('');
        setTransactionDescription('');
        setAmount('');
        setShowDeductionDetails(false);
    }

    // const addSourceOfIncomeDisabled = (): boolean =>
    //     !jobTitle
    //     || !frequencyId
    //     || recurringTransactions.length === 0;


    const onAddSourceOfIncomeClick = async () => {
        await props.addIncomeGenerator({
            description: jobTitle,
            frequencyId,
            recurringTransactions: [
                {
                    category: 'Gross pay',
                    amount: parseFloat(grossPay),
                    transactionTypeId: incomeTransactionTypeId,
                    frequencyId,
                    description: '',
                    lastTriggered
                },
                ...expenses.map(x => ({ ...x, frequencyId, lastTriggered: lastTriggered }))
            ]
        });
        props.push('/dashboard');
    }

    const frequencies: IDropdownOption<string>[] = props.frequencies.map(x => ({ displayValue: x.description, value: x.id }));

    return (
        <div className="add-income-generator">
            <h1 className="title">Add a source of income</h1>
            <div className="details">
                <h2 className="details-title">Details</h2>
                <TextField label="Job title" validator={descriptionValidator} value={jobTitle} onChange={value => setJobTitle(value)} />
                <TextField label="Gross pay" validator={numericValidator} preToken="$" value={grossPay} onChange={value => setGrossPay(value)} />
                <Dropdown label="Frequency" value={frequencyId} options={frequencies} onChange={value => setFrequencyId(value)} />
                <DateField label="Last payment" value={lastTriggered} onChange={date => handleDateChanged(date)} minDate={minDate} maxDate={new Date()} />
                <div className="add-income">
                    <img src={add} alt={"Add"} onClick={onAddSourceOfIncomeClick} />
                </div>
            </div>
            <div className="deductions">
                <h2 className="deductions-title">Deductions</h2>
                <div className="add-deduction-control" onClick={() => setShowDeductionDetails(!showDeductionDetails)}>
                    {showDeductionDetails ?
                        <img src={close} alt="Close" />
                        : <img src={add} alt="Add" />}
                </div>
                {showDeductionDetails &&
                    <div className="deduction-details">
                        <AutocompleteField label="Category" validator={categoryValidator} value={category} options={props.categories} onChange={setCategory} getOptions={props.getCategories} />
                        <TextField label="Description" validator={descriptionValidator} value={transactionDescription} onChange={setTransactionDescription} />
                        <TextField validator={numericValidator} label="Amount" value={amount} preToken="$" onChange={setAmount} />
                        <div className="add" >
                            <img src={add} alt="Add" onClick={onAddClick} />
                        </div>
                    </div>
                }
                <div className="deduction-list">
                    {expenses.map(x => <Deduction key={x.description} transaction={x} onClick={(value) => onDeleteClick(value)} />)}
                </div>
            </div>
        </div >
    );
}

const mapStateToProps = (state: AppDataState): Partial<IAddIncomeGeneratorProps> => {
    return {
        categories: state.ledger.categories,
        frequencies: state.ledger.frequencies,
        transactionTypes: state.ledger.transactionTypes
    };
}

export default connect(mapStateToProps, { getCategories, getFrequencies, getTransactionTypes, addIncomeGenerator, push })(AddIncomeGenerator as any);

interface IDeductionProps {
    transaction: IExpenseRequest;
    onClick: (value: string) => void;
}

const Deduction = (props: IDeductionProps) =>
    <div className="deduction-summary">
        <span className="text">
            {getDisplayText(props.transaction.category, props.transaction.description)}
        </span>
        <span className="remove" onClick={() => props.onClick(props.transaction.description)} >
            <img src={close} alt="Remove" />
        </span>
    </div >;