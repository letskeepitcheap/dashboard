import { push } from 'connected-react-router';
import React from 'react';
import { connect } from 'react-redux';
import Button from '../components/Button';
import AppDataState from '../store/AppDataState';

interface ILandingProps {
    push: typeof push;
}

const Landing = (props: ILandingProps) =>
    <div className="landing">
        <p>
            This is a budgeting tool that I made for myself. It's free to use,
            open source, and is always a work-in-progress.
        </p>
        <Button onClick={() => props.push('/signup')}>Sign up</Button>
    </div>
const mapStateToProps = (state: AppDataState): Partial<ILandingProps> => {
    return {};
}

export default connect(mapStateToProps, { push })(Landing as any);