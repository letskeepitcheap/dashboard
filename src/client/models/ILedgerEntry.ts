export default interface ILedgerEntry {
    id: string;
    category: string;
    description: string;
    amount: number;
    transactionType: string;
    recurringTransactionId: string;
    transactionDate: Date;
}