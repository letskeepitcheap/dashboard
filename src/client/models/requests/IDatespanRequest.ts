export default interface IDatespanRequest {
    start: Date;
    end: Date;
}