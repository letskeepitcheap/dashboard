export default interface ITransactionType {
    id: string;
    description: string;
}