export default interface IExpense {
    id: string;
    category: string;
    description: string;
    amount: number;
    frequencyId: string;
    transactionType: string;
    incomeGeneratorId: string;
    lastTriggered: Date;
}