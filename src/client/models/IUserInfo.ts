import UserRole from './UserRole';

export interface IUserInfo {
    username: string;
    role: UserRole;
}