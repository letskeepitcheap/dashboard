import { Route, Switch, withRouter } from 'react-router-dom';
import React from 'react';
import Navbar from './components/Navbar';
import About from './pages/About';
import AddExpense from './pages/AddExpense';
import Footer from './components/Footer';
import PrivateRoute from './components/PrivateRoute';
import Login from './pages/Login';
import Landing from './pages/Landing';
import AppDataState from './store/AppDataState';
import { connect } from 'react-redux';
import SignUp from './pages/SignUp';
import Dashboard from './pages/Dashboard';
import AddIncomeGenerator from './pages/AddIncomeGenerator';
import AddLedgerEntry from './pages/AddLedgerEntry';
import { IUserInfo } from './models/IUserInfo';
import './style/main.sass';
import { setRoute } from './store/user/actions';

interface AppProps {
    user: IUserInfo;
    setRoute: typeof setRoute;
}

const App = (props: AppProps) => {
    return (
        <div className="app">
            <Navbar />
            <Switch>
                <Route path="/login" component={Login} />
                <Route path="/signup" component={SignUp} />
                <PrivateRoute path="/dashboard" component={Dashboard} {...props} />
                <PrivateRoute path="/income/add" component={AddIncomeGenerator} {...props} />
                <PrivateRoute path="/ledger/add" component={AddLedgerEntry} {...props} />
                <PrivateRoute path="/expense/add" component={AddExpense} {...props} />
                <Route path="/about" component={About} />
                <Route path="/" component={Landing} />
            </Switch>
            <Footer />
        </div>
    );
}

const mapStateToProps = (state: AppDataState): Partial<AppProps> => {
    return {
        user: state.user.user
    };
}

export default withRouter(connect(mapStateToProps, { setRoute })(App as any));
