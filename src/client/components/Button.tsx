import React, { MouseEvent, useMemo } from "react";
import { FunctionComponent } from "react";

interface IButtonProps {
    onClick: () => void;
    disabled?: boolean;
    error?: boolean;
}

const Button: FunctionComponent<IButtonProps> = (props) => {

    const onClick = (event: MouseEvent) => {
        event.preventDefault();
        props.onClick();
    }

    const classes = useMemo(() => {
        const classes: string[] = [];
        if (props.error) { classes.push('error'); }
        return classes.join(' ');
    }, [props.error]);

    return (
        <button disabled={props.disabled} className={classes} onClick={(event) => onClick(event)}>
            {props.children}
        </button>
    );
}

export default Button;