import React from "react";
import { FunctionComponent } from "react";

interface IModalProps {
    show: boolean;
    close: () => void;
}

const Modal: FunctionComponent<IModalProps> = (props) => {

    const close = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
        event.preventDefault();
        props.close();
    }

    return props.show ?
        <div className="modal" onClick={(event) => close(event)}>
            <div className="modal-body" onClick={(event) => event.stopPropagation()}>
                {props.children}
            </div>
        </div>
        : null;
}

export default Modal;
