import React, { useMemo } from "react";
import { useState } from "react";
import { debounce } from "../utilities/utilities";

interface IAutocompleteFieldProps {
    label: string;
    options: string[];
    value: string;
    onChange: (value: string) => void;
    getOptions: (value: string) => void;
    validator: (value: string) => boolean;
}

const AutocompleteField = (props: IAutocompleteFieldProps) => {
    const [focused, setFocused] = useState(false);
    const [error, setError] = useState(false);

    const id = `autocomplete-${props.label}`;

    const getOptions = useMemo(() => debounce(props.getOptions), [props.options]);

    const onChange = (value: string) => {
        getOptions(value);
        props.onChange(value);
    }

    const onSelect = (value: string) => {
        setFocused(false);
        props.onChange(value);
        // Because this is onSelect, don't debounce.
        props.getOptions('');
    }

    const onInputBlur = () => {
        setError(props.validator(props.value));
        props.getOptions('');
        setFocused(false);
    }

    const containerClasses = useMemo(() => {
        // Re-using the styling for custom-dropdown
        const classes: string[] = ['dropdown', 'autocomplete'];
        if (focused) { classes.push('focused'); }
        if (props.value || focused) { classes.push('active'); }
        if (error) { classes.push('error'); }
        return classes.join(' ');
    }, [focused, props.value, error]);

    const labelClasses = useMemo(() => {
        const classes: string[] = [];
        if (focused || props.value) { classes.push('active'); }
        return classes.join(' ');
    }, [focused, props.value]);

    // I don't love that this is _almost_ duplicate code from Dropdown.tsx,
    // but the differences are really important. This component allows the 
    // user to type in it, which the Dropdown does not (and the Dropdown doesn't
    // handle change in the same way).
    return (
        <div className={containerClasses} onClick={() => document.getElementById(id)?.focus()} onFocus={() => setFocused(true)}>
            <label className={labelClasses} >{props.label}</label>
            <input id={id} type="text" value={props.value} onChange={event => onChange(event.target.value)} onBlur={onInputBlur} />
            {focused && props.options.length > 0 &&
                <div className="dropdown-menu">
                    {props.options.map(x =>
                        <div key={x} className="dropdown-item" onMouseDown={() => onSelect(x)}>
                            <span className="text">{x}</span>
                        </div>)
                    }
                </div>}
        </div>
    );
}

export default AutocompleteField;