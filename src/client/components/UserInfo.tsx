import React from "react";
import { passwordValidator } from "../utilities/validators";
import Button from "./Button";
import TextField from "./TextField";

interface IUserInfoProps {
    username: string;
    password: string;
    disabled?: boolean;
    buttonText: string;
    handleUsernameChanged: (value: string) => void;
    handlePasswordChanged: (value: string) => void;
    onSubmit: () => void;
}

const UserInfo = (props: IUserInfoProps) =>
    <form className="user-info">
        <TextField value={props.username} label="Username" onChange={(value) => props.handleUsernameChanged(value)} />
        <TextField validator={passwordValidator} value={props.password} label="Password" password onChange={(value) => props.handlePasswordChanged(value)} />
        <Button disabled={props.disabled} onClick={props.onSubmit}>{props.buttonText}</Button>
    </form>;

export default UserInfo;