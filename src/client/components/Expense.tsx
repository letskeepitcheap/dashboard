import React from "react";
import IExpense from "../models/IExpense";
import { getDisplayText } from "../utilities/text";

interface IExpenseProps {
    expense: IExpense;
    times: number;
    onClick?: (value: IExpense) => void;
}

const Expense = (props: IExpenseProps) =>
    <div className="expense" onClick={() => props.onClick ? props.onClick(props.expense) : undefined}>
        <div className="category">{getDisplayText(props.expense.category, props.expense.description)}</div>
        <div className="times">{`${props.times.toFixed(2)} x`}</div>
        <span className="amount">${props.expense.amount.toFixed(2)}</span>
    </div>;

export default Expense;