import React from "react";
import { Component } from "react";

export interface IRadioOption<T> {
    value: T;
    description: string;
}

interface IRadioButtonProps<T> {
    value: T;
    options: IRadioOption<T>[];
    onChange: (value: T) => void;
}

// TODO (alexa): this needs _actual_ radio buttons for accessability.
class RadioButton<T> extends Component<IRadioButtonProps<T>, never> {

    constructor(props: IRadioButtonProps<T>) {
        super(props);
    }

    render() {
        return (<div className="radio-button-container">
            {this.props.options.map((x, i) => {
                const checked: boolean = x.value === this.props.value;
                return (
                    <div key={i} className="radio-button" onClick={() => this.props.onChange(x.value)} >
                        {x.description}
                        {checked && <div className="accent" />}
                    </div>
                );
            })}
        </div>);
    }
}

export default RadioButton;