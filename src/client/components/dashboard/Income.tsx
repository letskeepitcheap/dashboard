import { push } from "connected-react-router"
import React, { useMemo } from "react"
import { useState } from "react"
import { connect } from "react-redux"
import IFrequency from "../../models/IFrequency"
import IIncome from "../../models/IIncome"
import IIncomeGenerator from "../../models/IIncomeGenerator"
import AppDataState from "../../store/AppDataState"
import { deleteIncomeGenerator } from "../../store/ledger/actions"
import { MONTHS } from "../../utilities/constants"
import { calculateIncome, getTotalIncomeGenerators } from "../../utilities/income_generators"
import RadioButton, { IRadioOption } from "../RadioButton"
import IncomeGeneratorModal from "./IncomeGeneratorModal"

// @ts-expect-error - The typescript compiler says that it can't find 
// the module for this import. Webpack still compiles it just fine.
import add from '../../assets/add.png';

interface IIncomeProps {
    incomeGenerators: IIncomeGenerator[];
    frequencies: IFrequency[];
    deleteIncomeGenerator: typeof deleteIncomeGenerator;
    push: typeof push;
}

const Income = (props: IIncomeProps) => {
    const [monthly, setMonthly] = useState(true);
    const [generator, setGenerator] = useState<IIncomeGenerator | undefined>(undefined);

    const income: IIncome = useMemo(() => getTotalIncomeGenerators(props.incomeGenerators, props.frequencies, monthly), [props.incomeGenerators, props.frequencies, monthly]);

    const calculateTotalClasses = (total: number): string => {
        const classes: string[] = ['value'];
        if (total <= 0) { classes.push('negative'); }
        return classes.join(' ');
    }

    // TODO (alexa): move this monthly setting to the redux store so it's not
    // duplicated everywhere.
    const options: IRadioOption<boolean>[] = [
        { value: true, description: MONTHS[new Date().getMonth()] },
        { value: false, description: new Date().getFullYear().toString() }
    ];

    return (
        <div className="income">
            <h1 className="title">Income</h1>
            <div className="add-income-generator">
                <img src={add} alt="Add" onClick={() => props.push('/income/add')} />
            </div>
            <div className="sources-of-income">
                {props.incomeGenerators.map(x => <SourceOfIncome key={x.id} generator={x} income={calculateIncome(x, props.frequencies, monthly)} onClick={setGenerator} />)}
            </div>
            <div className="income-total">
                <div className={`gross-${calculateTotalClasses(income.gross)}`}>${income.gross.toFixed(2)}</div>
                <div className="gross-text">Gross</div>
                <div className={`net-${calculateTotalClasses(income.net)}`}>${income.net.toFixed(2)}</div>
                <div className="net-text">Net</div>
            </div>
            <div className="term-selector">
                <RadioButton value={monthly} options={options} onChange={setMonthly} />
            </div>
            <IncomeGeneratorModal generator={generator} deleteIncomeGenerator={props.deleteIncomeGenerator} frequencies={props.frequencies} close={() => setGenerator(undefined)} />
        </div >
    )
}

const mapStateToProps = (state: AppDataState): Partial<IIncomeProps> => {
    return {
        incomeGenerators: state.ledger.incomeGenerators,
        frequencies: state.ledger.frequencies,
    };
}

export default connect(mapStateToProps, { deleteIncomeGenerator, push })(Income as any);

interface ISourceOfIncomeProps {
    generator: IIncomeGenerator;
    income: IIncome;
    onClick: (value: IIncomeGenerator) => void;
}

const SourceOfIncome = (props: ISourceOfIncomeProps) =>
    <div className="income-generator" onClick={() => props.onClick(props.generator)}>
        <span className="description">{props.generator.description}</span>
        <span className="gross">${props.income.gross.toFixed(2)}</span>
        <span className="net">${props.income.net.toFixed(2)}</span>
    </div>