import React from "react";
import ILedgerEntry from "../../models/ILedgerEntry";
import { deleteLedgerEntry } from "../../store/ledger/actions";
import { getDisplayText } from "../../utilities/text";
import Button from "../Button";
import Modal from "../Modal";

interface ILedgerEntryModalProps {
    entry?: ILedgerEntry;
    deleteLedgerEntry: typeof deleteLedgerEntry;
    close: () => void;
}

const LedgerEntryModal = (props: ILedgerEntryModalProps) => {

    const onDeleteClick = () => {
        props.deleteLedgerEntry(props.entry!.id);
        props.close();
    }

    return props.entry ?
        <Modal show={!!props.entry} close={props.close}>
            <h1>{getDisplayText(props.entry.category, props.entry.description)}</h1>
            <span className="amount">${props.entry!.amount.toFixed(2)}</span>
            <span className="date">{props.entry!.transactionDate.toReadable()}</span>
            <Button onClick={onDeleteClick} error>
                Delete
            </Button>
        </Modal>
        : null
}

export default LedgerEntryModal;