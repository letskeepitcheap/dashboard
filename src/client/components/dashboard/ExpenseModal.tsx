import React from "react";
import IExpense from "../../models/IExpense";
import IFrequency from "../../models/IFrequency";
import { deleteExpense } from "../../store/ledger/actions";
import { getDisplayText } from "../../utilities/text";
import Button from "../Button";
import Modal from "../Modal";

interface IExpenseModalProps {
    expense?: IExpense;
    frequencies: IFrequency[];
    deleteExpense: typeof deleteExpense;
    close: () => void;
}

const ExpenseModal = (props: IExpenseModalProps) => {

    const getFrequencyDescription = (): string => {
        const selected = props.frequencies.find(x => x.id === props.expense?.frequencyId);
        if (!selected) {
            props.close();
            return '';
        }
        return selected.description;
    }

    const onDeleteClick = async () => {
        props.deleteExpense(props.expense!.id);
        props.close();
    }

    return props.expense ?
        <Modal show={!!props.expense} close={props.close}>
            <h1>{getDisplayText(props.expense.category, props.expense.description)}</h1>
            <span className="frequency">{getFrequencyDescription()}</span>
            <span className="amount">${props.expense!.amount.toFixed(2)}</span>
            <Button onClick={onDeleteClick} error>
                Delete
            </Button>
        </Modal>
        : null
}

export default ExpenseModal;