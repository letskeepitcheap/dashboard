import { fireEvent, render, RenderResult } from "@testing-library/react"
import Dropdown from "../../../client/components/Dropdown";
import '@testing-library/jest-dom';
import React from "react";

describe("Dropdown", () => {
    let utils: RenderResult;

    const defaultProps = {
        value: 'one',
        label: 'test-label',
        options: [
            { value: 'one', displayValue: 'display-1' },
            { value: 'two', displayValue: 'display-2' }
        ],
        onChange: () => undefined
    };
    const spy = jest.spyOn(defaultProps, 'onChange');


    beforeEach(() => {
        utils = render(<Dropdown {...defaultProps} />);
    });

    it('should have expected structure', () => {
        const { container } = utils;

        expect(container.firstChild).toHaveClass('dropdown');

        const dropdown = container.firstChild!;
        // The dropdown is closed by default.
        expect(dropdown.childNodes.length).toBe(3);
        expect(dropdown.childNodes[1]).toHaveValue('display-1');
        expect(dropdown.childNodes[2]).toHaveClass('icon');
    });

    it('should open when label clicked', () => {
        const { container } = utils;

        const dropdownContainer = container.firstChild;

        fireEvent.click(dropdownContainer!.firstChild!);
        expect(dropdownContainer?.childNodes.length).toBe(4);
        expect(dropdownContainer?.childNodes[3]).toHaveClass('dropdown-menu');

        const menu = dropdownContainer?.childNodes[3];
        expect(menu?.childNodes.length).toBe(defaultProps.options.length);

        expect(dropdownContainer).toHaveClass('open');
    });

    it('should call props.onChange and close when option is selected', () => {
        const { container } = utils;

        const dropdownContainer = container.firstChild;

        // Open the dropdown
        fireEvent.click(dropdownContainer?.firstChild!);

        const menu = dropdownContainer?.childNodes[3];
        const index = Math.floor(Math.random() * defaultProps.options.length);

        fireEvent.mouseDown(menu!.childNodes[index]);

        expect(dropdownContainer).not.toHaveClass('open');
        expect(spy).toHaveBeenCalledTimes(1);
    });
});
