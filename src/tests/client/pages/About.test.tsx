import '@testing-library/jest-dom';
import React from "react";
import { render, RenderResult } from "@testing-library/react"
import About from '../../../client/pages/About';

describe('About', () => {
    let utils: RenderResult;

    beforeEach(() => {
        utils = render(<About />);
    });

    it('should have expectedstructure', () => {
        const { container } = utils;

        const about = container.firstChild;
        expect(about).toHaveClass('about');
        expect(about.childNodes.length).toBe(2);

        expect(about.childNodes[0]).toHaveTextContent(/credits/i);
        expect(about.childNodes[1].childNodes.length).toBe(2);
    });
});