FROM node:14 as build
ARG PORT=80

WORKDIR /src
COPY . .
RUN npm install

ARG NODE_ENV=production
RUN echo PORT=$PORT >> .env 

RUN npm run build

FROM build as execute 
WORKDIR /src/dist
CMD ["npm", "start"]